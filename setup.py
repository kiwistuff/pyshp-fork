from setuptools import setup

setup(name='pyshp-fork',
      version='1.2.4',
      description='Pure Python read/write support for ESRI Shapefile format',
      long_description=open('README.txt').read(),
      author='Joel Lawhead with modifications by John Burnell',
      author_email='j.burnell(at)gns.cri.nz',
      url='https://kiwistuff@bitbucket.org/kiwistuff/pyshp-fork.git',
      download_url='https://bitbucket.org/kiwistuff/pyshp-fork/downloads',
      py_modules=['shapefile'],
      license='MIT',
      zip_safe=False,
      keywords='gis geospatial geographic shapefile shapefiles',
      classifiers=['Programming Language :: Python',
                   'Topic :: Scientific/Engineering :: GIS',
                   'Topic :: Software Development :: Libraries',
                   'Topic :: Software Development :: Libraries :: Python Modules'])
